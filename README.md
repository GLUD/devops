# Certificación LPI

### Director Actual:
- [Angel Granados](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos/Angel_Granados?ref_type=heads)

### [Proyectos](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos?ref_type=heads)

## Implementación del Programa de Certificación LPI en el GLUD

El objetivo principal es establecer un programa de certificación en el Grupo de Linux de la Universidad Distrital (GLUD) en colaboración con la Universidad y el Linux Professional Institute (LPI). Este programa busca dotar a los estudiantes del GLUD de la Universidad Distrital Francisco José de Caldas con habilidades y conocimientos avanzados en tecnologías GNU/Linux, promoviendo así su desarrollo profesional en el ámbito de la informática.

En el contexto actual, la adopción de sistemas basados en Linux ha experimentado un crecimiento significativo en la industria de la tecnología de la información. La demanda de profesionales capacitados en Linux es cada vez mayor, y la obtención de certificaciones reconocidas, como las ofrecidas por el LPI, se ha convertido en un diferenciador crucial en el mercado laboral.

El GLUD, como entidad comprometida con la formación integral de sus miembros, propone la implementación de un programa de certificación que permita a los estudiantes de la Universidad Distrital adquirir competencias técnicas en entornos Linux, fortaleciendo así su perfil profesional y facilitando su inserción en el mundo laboral.


## Mapa de Certificaciones:

![](https://gitlab.com/GLUD/devops/-/raw/main/image/mapa.png?ref_type=heads)

## Linux Professional Institute Linux Essentials

![](https://gitlab.com/GLUD/devops/-/raw/main/image/Essentials-Linux.png?ref_type=heads)

La adopción de Linux continúa aumentando en todo el mundo a medida que usuarios individuales, entidades gubernamentales e industrias que van desde la automoción hasta la exploración espacial adoptan tecnologías de código abierto. Esta expansión del código abierto en las empresas está redefiniendo los roles laborales tradicionales de Tecnologías de la Información y la Comunicación (TIC) para requerir más habilidades en Linux. Ya sea que esté comenzando su carrera en código abierto o buscando avanzar, verificar de forma independiente su conjunto de habilidades puede ayudarlo a destacar ante los gerentes de contratación o su equipo gerencial.

El certificado Linux Essentials también sirve como una excelente introducción al camino de certificación Linux Professional, más completo y avanzado.

**Versión actual:** 1.6 (código de examen 010-160)

**Objetivos:** 010-160

**Requisitos previos:** No existen requisitos previos para esta certificación en la pagina LPI pero en el GLUD (Grupo de Linux ) se requiere lo siguiente:

- Estudiantes Activos en el Grupo LPI 

- Superación al 90% de las pruebas locales en el Grupo LPI como miembros.

- Aprobación al 90% de la prueba simulada por el LPI antes de realizar la prueba en forma.
	   

**Requerimientos:** Aprobar el examen Linux Essentials 010. El examen Linux Essentials contiene 40 preguntas y debe completarse en 60 minutos.

**Periodo de validez:** De por vida (En el periodo 2024)

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, francés, alemán, japonés, portugués (brasileño), español

**Idiomas para el examen disponibles en línea a través de OnVUE:** inglés, francés, alemán, japonés, portugués (brasileño) y español.

### **Para recibir el certificado Linux Essentials el candidato debe demostrar:**

- Tener conocimientos de la industria de Linux y del código abierto y conocimiento de las aplicaciones de código abierto más populares;
     
- Comprender los componentes principales del sistema operativo Linux y tener la competencia técnica para trabajar en la línea de comandos de Linux; y

- Tener conocimientos básicos de temas relacionados con la seguridad y la administración, como la gestión de usuarios/grupos, el trabajo en la línea de comandos y los permisos.

## Linux Professional Institute Security Essentials

![](https://gitlab.com/GLUD/devops/-/raw/main/image/security-essentials-mockup_0.png?ref_type=heads)

La seguridad de TI es absolutamente crucial para las personas y las organizaciones en el mundo digital actual. La capacidad general de proteger datos, dispositivos y redes es una habilidad fundamental para el uso responsable de las tecnologías de la información. Obtener el certificado Linux Professional Institute Security Essentials es una manera fantástica de aprender cómo protegerse y demostrar su conocimiento y experiencia en el campo a posibles empleadores y clientes.

El examen Security Essentials cubre conocimientos preliminares en todos los campos importantes de la seguridad de TI. El certificado está destinado a estudiantes que realizaron su primer curso en seguridad informática, a todos los miembros y personal de organizaciones que deseen mejorar su seguridad informática, así como a personas que deseen adquirir una competencia básica en el uso seguro de la tecnología de la información.

**Versión actual:** 1.0 (código de examen 020-100)

**Objetivos:** 020-100

**Requisitos previos:** No existen requisitos previos para esta certificación.

**Requerimientos:** Aprobar el examen Security Essentials 020

**Formato del examen:** 40 preguntas y debe completarse en 60 minutos.

**Periodo de validez:** De por vida

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés y japonés

**Idiomas para el examen disponibles en línea a través de OnVUE:** inglés y japonés

### **Para recibir el certificado Security Essentials, el candidato debe:**

- Tener un conocimiento básico de las amenazas de seguridad comunes derivadas del uso de computadoras, redes, dispositivos conectados y servicios de TI en las instalaciones y en la nube;
     
- Comprender formas comunes de prevenir y mitigar ataques contra sus dispositivos y datos personales;
     
- Poder utilizar el cifrado para proteger los datos transferidos a través de una red y almacenados en dispositivos de almacenamiento y en la nube;
     
- Ser capaz de aplicar las mejores prácticas de seguridad comunes, proteger la información privada y asegurar su identidad; y
     
- Capacidad de utilizar de forma segura los servicios de TI y asumir la responsabilidad de proteger sus dispositivos informáticos personales, aplicaciones, cuentas y perfiles en línea.

### **La seguridad de TI es absolutamente crucial para personas y organizaciones. El certificado Security Essentials valida una comprensión demostrada de lo siguiente:**

- Security Concepts
- Encryption
- Node, Device and Storage Security
- Network and Service Security
- Identity and Privacy

[Ver detalles de los objetivos del Examen](https://www.lpi.org/our-certifications/exam-020-objectives)

[Ver recursos de estudio de exámenes](https://learning.lpi.org/en/learning-materials/020-100/)

## Linux Professional Institute Web Development Essentials

![](https://gitlab.com/GLUD/devops/-/raw/main/image/Essentials-Web-Development.png?ref_type=heads)


Las aplicaciones de software modernas se desarrollan comúnmente para la Web. El programa Web Development Essentials de Linux Professional Institute (LPI) respalda sus primeros pasos en el desarrollo de software. Incluye materiales de aprendizaje adecuados tanto para la formación como para el autoestudio. Al aprobar el examen Web Development Essentials, recibirá un certificado para demostrar sus habilidades.

Los objetivos del programa cubren los aspectos más importantes del desarrollo web. Están diseñados específicamente para incluir todo lo necesario para implementar aplicaciones web simples. Esto hace que Web Development Essentials sea la elección perfecta para cursos prácticos y formación. En el autoestudio, los candidatos encuentran todo lo que necesitan para lograr un éxito inmediato en la implementación de sus primeros proyectos.

**Versión actual:** 1.0 (código de examen 030-100)

**Objetivos:** 030-100

**Requisitos previos:** Ninguno, cualquiera puede realizar el examen.

**Formato del examen:** 40 preguntas en 60 minutos

**Periodo de validez:** De por vida

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés y japonés

**Idiomas para el examen disponibles en línea a través de OnVUE:** inglés y japonés

### **Para recibir el certificado Web Development Essentials, el candidato debe:**

Tener conocimientos de los principios del desarrollo de software, HTML, CSS, JavaScript, Node.js y SQL.

## Linux Professional Institute LPIC-1

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-1_0.png?ref_type=heads)

LPIC-1 es la primera certificación en el programa de certificación profesional de Linux multinivel del Linux Professional Institute (LPI). El LPIC-1 validará la capacidad del candidato para realizar tareas de mantenimiento en la línea de comandos, instalar y configurar una computadora con Linux y configurar redes básicas.

El LPIC-1 está diseñado para reflejar la investigación actual y validar la competencia de un candidato en la administración de sistemas del mundo real. Los objetivos están vinculados a habilidades laborales del mundo real, que determinamos mediante encuestas de análisis de tareas laborales durante el desarrollo del examen.

**Versión actual:** 5.0 (códigos de examen 101-500 y 102-500)

**Objetivos:** 101-500, 102-500

**Requisitos previos:** No existen requisitos previos para esta certificación.

**Requerimientos:** Aprobar los exámenes 101 y 102. Cada examen de 90 minutos consta de 60 preguntas de opción múltiple y para completar espacios en blanco.

**Periodo de validez:** 5 años a menos que se repita o se alcance un nivel superior.

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, alemán, japonés, portugués (brasileño), chino (simplificado), chino (tradicional) y español (moderno)

**Idiomas para el examen disponibles en línea a través de OnVUE:** inglés, alemán, japonés, portugués (brasileño) y español

### **Para obtener la certificación LPIC-1, el candidato debe poder:**

- Comprender la arquitectura de un sistema Linux;

- Instalar y mantener una estación de trabajo Linux, incluido X11, y configurarla como cliente de red;

- Trabajar en la línea de comandos de Linux, incluidos los comandos comunes de GNU y Unix;

- Manejar archivos y permisos de acceso, así como la seguridad del sistema; y

- Realice tareas de mantenimiento sencillas: ayude a los usuarios, agregue usuarios a un sistema más grande, realice copias de seguridad y restaure, apague y reinicie.

[Enlace de recursos LPIC 1 - 101](https://learning.lpi.org/en/learning-materials/101-500/)

[Enlace de recursos LPIC 1 - 102](https://learning.lpi.org/en/learning-materials/102-500/)

[Objetivos del Examen-101](https://www.lpi.org/our-certifications/exam-101-objectives)

[Objetivos del Examen-102](https://www.lpi.org/our-certifications/exam-102-objectives)

## Linux Professional Institute LPIC-2

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-2_0.png?ref_type=heads)

LPIC-2 es la segunda certificación en el programa de certificación profesional multinivel del Linux Professional Institute (LPI). El LPIC-2 validará la capacidad del candidato para administrar redes mixtas de tamaño pequeño y mediano.

**Versión actual:** 4.5 (códigos de examen 201-450 y 202-450)

**Objetivos:** 201-450, 202-450

**Requisitos previos:** El candidato debe tener una certificación LPIC-1 activa para recibir la certificación LPIC-2.

**Requerimientos:** Aprobar los exámenes 201 y 202. Cada examen de 90 minutos consta de 60 preguntas de opción múltiple y espacios en blanco.

**Periodo de validez:** 5​ años a menos que se repita o se alcance un nivel superior.

**Idiomas para los exámenes disponibles en los centros de pruebas VUE:** inglés, alemán, japonés, portugués (brasileño)

**Idiomas para los exámenes disponibles en línea a través de OnVUE:** inglés, japonés

### **Para obtener la certificación LPIC-2, el candidato debe poder:**

- Realizar administración avanzada del sistema, incluidas tareas comunes relacionadas con el kernel de Linux, el inicio y el mantenimiento del sistema;

- Realizar una gestión avanzada de sistemas de archivos y almacenamiento en bloque, así como redes avanzadas, autenticación y seguridad del sistema, incluidos firewall y VPN;

- Instalar y configurar servicios de red fundamentales, incluidos DHCP, DNS, SSH, servidores web, servidores de archivos mediante FTP, NFS y Samba, entrega de correo electrónico; y

- Supervisar asistentes y asesorar a la gerencia en automatización y compras.

[Enlace de recursos LPIC 2](https://learning.lpi.org/en/learning-materials/all-materials/)

[Objetivos del Examen-201](https://www.lpi.org/our-certifications/exam-201-objectives)

[Objetivos del Examen-202](https://www.lpi.org/our-certifications/exam-202-objectives)

## Linux Professional Institute LPIC-3 Mixed Environments

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-3-300-03_0.png?ref_type=heads)

La certificación LPIC-3 es la culminación del programa de certificación profesional multinivel del Linux Professional Institute (LPI). LPIC-3 está diseñado para el profesional de Linux de nivel empresarial y representa el nivel más alto de certificación profesional de Linux con distribución neutral dentro de la industria. Se encuentran disponibles cuatro certificaciones de especialidad LPIC-3 independientes. Aprobar cualquiera de los cuatro exámenes otorgará la certificación LPIC-3 para esa especialidad.

La certificación LPIC-3 Entornos Mixtos cubre la administración de sistemas Linux en toda la empresa en entornos mixtos.

**Versión actual:** 3.0 (código de examen 300-300)

**Versión anterior:** 1.0 (código de examen 300-100)
Disponible hasta el 23 de febrero de 2022

**Objetivos:** 300-300

**Requisitos previos:** El candidato debe tener una certificación LPIC-2 activa para recibir la certificación LPIC-3.

**Requerimientos:** Aprobar el examen 300. El examen de 90 minutos consta de 60 preguntas de opción múltiple y para completar espacios en blanco.

**Período de validez:** 5 años

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, japonés

**Idiomas para los exámenes disponibles en línea a través de OnVUE:** inglés, japones

[Objetivos del Examen Mixed Environments](https://www.lpi.org/our-certifications/exam-300-objectives)

## Linux Professional Institute LPIC-3 Security

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-3-303_0.png?ref_type=heads)

La certificación LPIC-3 es la culminación del programa de certificación profesional multinivel del Linux Professional Institute (LPI). LPIC-3 está diseñado para el profesional de Linux de nivel empresarial y representa el nivel más alto de certificación profesional de Linux con distribución neutral dentro de la industria. Se encuentran disponibles cuatro certificaciones de especialidad LPIC-3 independientes. Aprobar cualquiera de los cuatro exámenes otorgará la certificación LPIC-3 para esa especialidad.

La certificación de seguridad LPIC-3 cubre la administración de sistemas Linux en toda la empresa con énfasis en la seguridad.

**Versión actual:** 3.0 (código de examen 303-300)

**Objetivos:** 303-300

**Requisitos previos:** El candidato debe tener una certificación LPIC-2 activa para recibir la certificación LPIC-3.

**Requisitos:** Aprobar el examen 303. El examen de 90 minutos tiene 60 preguntas de opción múltiple y completa los espacios en blanco.

**Período de validez:** 5 años

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, japonés

**Idiomas para los exámenes disponibles en línea a través de OnVUE:** inglés, japonés

[Objetivos del Examen-303](https://www.lpi.org/our-certifications/exam-303-objectives)

## Linux Professional Institute LPIC-3 Virtualization and Containerization

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-3-305_1.png?ref_type=heads)

La certificación LPIC-3 es la culminación del programa de certificación profesional multinivel del Linux Professional Institute (LPI). LPIC-3 está diseñado para el profesional de Linux de nivel empresarial y representa el nivel más alto de certificación profesional de Linux con distribución neutral dentro de la industria. Se encuentran disponibles cuatro certificaciones de especialidad LPIC-3 independientes. Aprobar cualquiera de los cuatro exámenes otorgará la certificación LPIC-3 para esa especialidad.

La certificación LPIC-3 Virtualización y Contenedorización cubre la administración de sistemas Linux en toda la empresa con énfasis en la virtualización y la contenedorización.

**Versión actual:** 3.0 (código de examen 305-300)

**Objetivos:** 305-300

**Requisitos previos:** El candidato debe tener una certificación LPIC-2 activa para recibir la certificación LPIC-3.

**Requerimientos:** Aprobar el examen 305. El examen de 90 minutos tiene 60 preguntas de opción múltiple y completa los espacios en blanco.

**Período de validez:** 5 años

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, japonés

**Idiomas para los exámenes disponibles en línea a través de OnVUE:** inglés, japonés

[Objetivos del Examen-305](https://www.lpi.org/our-certifications/exam-305-objectives)

## Linux Professional Institute LPIC-3 High Availability and Storage Clusters

![](https://gitlab.com/GLUD/devops/-/raw/main/image/LPIC-3-306.png?ref_type=heads)

La certificación LPIC-3 es la culminación del programa de certificación profesional multinivel del Linux Professional Institute (LPI). LPIC-3 está diseñado para el profesional de Linux de nivel empresarial y representa el nivel más alto de certificación profesional de Linux con distribución neutral dentro de la industria. Se encuentran disponibles cuatro certificaciones de especialidad LPIC-3 independientes. Aprobar cualquiera de los cuatro exámenes otorgará la certificación LPIC-3 para esa especialidad.

La certificación LPIC-3 Clústeres de almacenamiento y alta disponibilidad cubre la administración de sistemas Linux en toda la empresa con énfasis en sistemas y almacenamiento de alta disponibilidad.

**Versión actual:** 3.0 (código de examen: 306-300)

**Objetivos:** 306-300

**Requisitos previos:** El candidato debe tener una certificación LPIC-2 activa para recibir la certificación LPIC-3.

**Requerimientos:** Aprobar el examen 306. El examen de 90 minutos tiene 60 preguntas de opción múltiple y completa los espacios en blanco.

**Período de validez:** 5 años

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, japonés

**Idiomas para el examen disponibles en línea a través de OnVUE:** inglés, japonés

[Objetivos del Examen-303](https://www.lpi.org/our-certifications/exam-306-objectives)

## Linux Professional Institute DevOps Tools Engineer

![](https://gitlab.com/GLUD/devops/-/raw/main/image/DevOpsToolsEngineer.png?ref_type=heads)

Las empresas de todo el mundo están implementando cada vez más prácticas de DevOps para optimizar las tareas diarias de administración de sistemas y desarrollo de software. Como resultado, empresas de todos los sectores están contratando profesionales de TI que puedan aplicar DevOps de forma eficaz para reducir el tiempo de entrega y mejorar la calidad en el desarrollo de nuevos productos de software.

Para satisfacer esta creciente necesidad de profesionales calificados, Linux Professional Institute (LPI) desarrolló la certificación Linux Professional Institute DevOps Tools Engineer que verifica las habilidades necesarias para utilizar las herramientas que mejoran la colaboración en los flujos de trabajo durante la administración de sistemas y el desarrollo de software.

Al desarrollar la certificación de ingeniero de herramientas DevOps del Linux Professional Institute, LPI revisó el panorama de las herramientas DevOps y definió un conjunto de habilidades esenciales al aplicar DevOps. Como tal, el examen de certificación se centra en las habilidades prácticas necesarias para trabajar con éxito en un entorno DevOps, centrándose en las habilidades necesarias para utilizar las herramientas DevOps más destacadas. El resultado es una certificación que cubre la intersección entre desarrollo y operaciones, lo que la hace relevante para todos los profesionales de TI que trabajan en el campo de DevOps.

**Versión actual:** 1.0 (código de examen 701-100)

**Objetivos:** 701-100

**Requisitos previos:** No existen requisitos previos para esta certificación. Sin embargo, se recomienda encarecidamente una certificación adicional en el área principal de especialización del candidato, como LPIC-1 o una certificación de desarrollador.

**Requerimientos:** Aprobar el examen de Ingeniero de Herramientas DevOps. El examen de 90 minutos consta de 60 preguntas de opción múltiple y para completar los espacios en blanco.

**Período de validez:** 5 años

**Idiomas para el examen disponibles en los centros de pruebas VUE:** inglés, japonés

**Idiomas para los exámenes disponibles en línea a través de OnVUE:** inglés, japonés

### **Para recibir la certificación de Ingeniero de Herramientas DevOps de Linux Professional Institute, el candidato debe:**

- Tener un conocimiento práctico de dominios relacionados con DevOps, como ingeniería y arquitectura de software, implementación de contenedores y máquinas, gestión y monitoreo de configuración.

- Tener competencia en destacadas utilidades gratuitas y de código abierto, como Docker, Vagrant, Ansible, Puppet, Git y Jenkins.