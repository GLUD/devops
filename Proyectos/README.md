# Requisitos Para la Certificación

![](https://gitlab.com/GLUD/devops/-/raw/main/image/lpi_logo.jpg?ref_type=heads)

## Integrantes Activos con sus proyectos:

- [Angel Granados](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos/Angel_Granados?ref_type=heads)
- [Juan Bedoya](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos/Juan_Bedoya?ref_type=heads)
- [Alejandro Cortazar](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos/Alejandro_Cortazar?ref_type=heads)
- [Andres Combaria](https://gitlab.com/GLUD/devops/-/tree/main/Proyectos/Andres_Combaria?ref_type=heads)

## Metas 2024 I-Semestre:

- Proyectos Semanales  (Exposiciones Martes)

- Ejercicios por carpetas individuales (Con sus Nombres)

- Simulación con aprobación (Mayor al 90% individual)

- Prueba Real.

Cualquier recurso es viable en la web y material físico.